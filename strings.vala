bool startsWith(string s, string start)
{
	return (s.length >= start.length && s.substring(0, start.length) == start);
}

void main(string[] args)
{
	var line = "Aug 25 07:37:54 host kernel: [230149.885426] NASTY IN=eth3 OUT=eth4";
	var inInterface = "", outInterface = "";
	foreach (var item in line.split(" "))
    {
        if (startsWith(item, "IN="))
        {
            inInterface = item.substring(3);
        }
        else if (startsWith(item, "OUT="))
        {
            outInterface = item.substring(4);
        }
	}
	print(inInterface + " " + outInterface + "\n");
}
